# Informatique

## Environnement
- [Articles de l'Institut National de Recherche en Informatique et en Automatique](https://www.inria.fr/fr/numerique-frugal)
- [Articles du Centre National de la Recherche Scientifique](https://lejournal.cnrs.fr/numerique)
- [Articles du journalduhacker libellés environnement](https://www.journalduhacker.net/t/environnement)
- [Entreprises du numérique engagées pour l'environnement](https://www.impactfrance.eco/communautes-thematiques/tech-for-good)
- [Quelques bonnes pratiques pour limiter les coûts énergétiques liés au code et au réseau](https://www.richard-dern.fr/blog/2021/09/25/l-eco-responsabilite-en-informatique/)
- [Eco-conception du web](https://www.webdesignfortheplanet.com/)
- [Intégrer le coût énergétique dans les métriques de fiabilité](https://bpetit.nce.re/fr/2021/02/les-pratiques-sre-et-le-climat/)
- [Résumé de bonnes pratiques individuelles](https://interstices.info/wp-content/uploads/2020/12/cout-internet-energie_figure-11_800x654-768x628.jpg) [issue de l'ADEME](https://librairie.ademe.fr/consommer-autrement/249-comment-teletravailler-leger-.html) et [l'article l'accompagnant](https://interstices.info/le-vrai-cout-energetique-du-numerique/)
- [Pistes d'améliorations pour l'hébergement](https://blog.link-value.fr/datacenters-ecoresponsables-impact-hebergement-donnees-aed671e415bd), par exemple favoriser les refroidissements passifs (couloir froid, toiture blanche...), utiliser la chaleur produite (chauffer les bureaux, connection aux réseaux de la ville...)
- Limiter les ressources d'un site web :
  - [Images : format et résolution](https://trolliet.info/blog/038-tailleimage/)
  - bibliothèques javascript et le concaténer puis [suppression de code mort avec webpack/rollup](https://developer.mozilla.org/en-US/docs/Glossary/Tree_shaking)
  - CSS [Onglet chrome pour détection du JS/CSS mort](https://developer.chrome.com/docs/devtools/coverage/)
- Charger que le nécessaire via notamment le LazyLoading
- Ne récupérer que le nécessaire via GraphQL

### Vidéos du BreizhCamp 2022
- [Impacts environnementaux du Cloud : the good, the bad, the ugly (Sylvain Révéreault)](https://www.youtube.com/watch?v=nWpjBElyxbs&list=PLv7xGPH0RMUS0sDzj_45G9F-iVWWck5kz&index=36)
- L'impact du langage [L'eco-conception, c'est bien, mais si on parlait un peu du backend ? (Jérémie Drouet, Youen Chéné)](https://www.youtube.com/watch?v=gE6HUsmh554&list=PLv7xGPH0RMUS0sDzj_45G9F-iVWWck5kz&index=52)
- Quelques outils de base pour suivre la consommation de ressources [Comment avoir une idée des
perfs de votre serveur Linux en 60s (Laurent Huet)](https://www.youtube.com/watch?v=mEuH_iz1-h8&list=PLv7xGPH0RMUS0sDzj_45G9F-iVWWck5kz&index=37)

### Outils
- [API de mesure de consommation énergétique](http://www.powerapi.org/)
- [Outil de mesure de consommation énergétique](https://github.com/hubblo-org/scaphandre)
- [Topic energy efficiency de github](https://github.com/topics/energy-efficiency) voir ceux de
[scaphandre](https://github.com/hubblo-org/scaphandre)
- [Présentation d'EcoWatt](https://www.geeek.org/ecowatt-api-alerte-electricite/)
- [EcoWatt : API RTE pour suivre la production/consommation électrique](https://data.rte-france.com/catalog/-/api/doc/user-guide/Ecowatt/4.0)
  - utiliser la [recherche](https://data.rte-france.com/search) pour connaître la dernière version
- [Articles et outils de boavizta](https://boavizta.org/)
